//
//  Cache.swift
//  Instruments
//
//  Created by TQI on 08/11/18.
//  Copyright © 2018 Luis Teodoro. All rights reserved.
//

import UIKit

class ImageCache {
    
    private static let _shared = ImageCache()
    
    var images = [String:UIImage]()
    
    static var shared: ImageCache {
        return _shared
    }
    
    //ALLOCATIONS
    
//    init() {
//        NotificationCenter.default.addObserver(forName: Notification.Name.UIApplicationDidReceiveMemoryWarning, object: nil, queue: .main) { [weak self] notification in
//            self?.images.removeAll(keepingCapacity: false)
//        }
//    }
//
//    deinit {
//        NotificationCenter.default.removeObserver(self)
//    }
   
    func loadThumbnail(for photo: FlickrPhoto, completion: @escaping FlickrAPI.FetchImageCompletion) {
        FlickrAPI.loadImage(for: photo, withSize: "m") { result in
            completion(result)
        }
    }
    
    //TIME PROFILER
    
//    func loadThumbnail(for photo: FlickrPhoto, completion: @escaping FlickrAPI.FetchImageCompletion) {
//        if let image = ImageCache.shared.image(forKey: photo.id) {
//            completion(Result.success(image))
//        }
//        else {
//            FlickrAPI.loadImage(for: photo, withSize: "m") { result in
//                if case .success(let image) = result {
//                    ImageCache.shared.set(image, forKey: photo.id)
//                }
//                completion(result)
//            }
//        }
//    }
}

// MARK: - Custom Accessors

extension ImageCache {
    
    func set(_ image: UIImage, forKey key: String) {
        images[key] = image
    }
    
    func image(forKey key: String) -> UIImage? {
        return images[key]
    }
}
