//
//  Result.swift
//  Instruments
//
//  Created by TQI on 08/11/18.
//  Copyright © 2018 Luis Teodoro. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case failure(Error)
}
